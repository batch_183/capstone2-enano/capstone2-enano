const jwt = require ("jsonwebtoken");

const secret = "secret";

module.exports.createAccessToken = (user) =>{
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }

    return jwt.sign (data, secret, {})
}
module.exports.verify = (req, res, next) =>{
	let token = req.headers.authorization;
	console.log(typeof token);

	if(typeof token !== "undefined"){
		console.log(token); 
		// This removes the "Bearer " prefix and obtains only the token for verification.
		token = token.slice(7, token.length);
		console.log(token);

	 	// Syntax: jwt.verify(token, secretCode, [options/callBackFunction])
		return jwt.verify(token, secret, (err, data)=>{
			// If JWT is not valid
			if(err){
				return res.send({auth: "Token Failed"});
			}
			else{
				// the verify method will be used as a middleware in the route to verify the token before proceeding to the function that invokes the controller function.
				next();
			}
		})
	}
	else{
		return res.send({auth: "Failed"});
	}
}

module.exports.decode = (token) =>{

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data)=>{
			// If JWT is not valid
			if(err){
				return null;
			}
			else{
				// Syntax: jwt.decode(token, [options])
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
	else{
		return null;
	}

}