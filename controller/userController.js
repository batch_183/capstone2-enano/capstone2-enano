const User = require ("../model/User");
const bcrypt = require ("bcryptjs");
const auth = require("../auth");
const Product = require("../model/Product");

module.exports.registerUser = (reqBody) => {    
    let newUser = new User ({
        email : reqBody.email,
        password : bcrypt.hashSync(reqBody.password,10),
    })

    return newUser.save().then((user, error) =>{   
      
        if (error){
            return "unable to register user account";
        }
        else {
            console.log(User);
            return "new user registered";            
        }
    })

}

module.exports.loginUser = (reqBody) => {
    return User.findOne({email: reqBody.email}).then(result=>{
        if (result == null){
            return false;
        }

        else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            if (isPasswordCorrect ){
                return {access: auth.createAccessToken(result)}
            }

            else {
                return false;
            }
        }
    })
}



