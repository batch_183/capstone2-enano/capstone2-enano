const { default: mongoose } = require("mongoose");
const Order = require("../model/Order");
const Product = require("../model/Product")
const User = require("../model/User")
// Adding Order


module.exports.createOrder = (reqBody) =>{
    const newOrder = new Order({
        _id: mongoose.Types.ObjectId(), 
        userId: reqBody.userId,
        productId: reqBody.productId, 
        quantity: reqBody.quantity
           
    });

    return newOrder.save(newOrder).then((result, error) =>{   
      
        if (error){
            return "unable to proceed with the order"
        }
        else {
            console.log(result);
            return "order has been created"
        }
    })
}

// Retrieve orders (authenticated users)
module.exports.usersorder = (userId) =>{
	return Order.find().then(result => result);
}

// Retrieve all orders (Admin)

module.exports.getallorders = () =>{
	return Order.find().then(result => result);
}