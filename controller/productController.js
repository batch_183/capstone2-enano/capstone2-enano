const Product = require ("../model/Product");

// Adding new product
module.exports.addProduct = (reqBody) =>{

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks,
	})

	return newProduct.save().then((product, error) =>{
		if(error){
			return "error occurs while adding a product"
		}
		else{
			return true 
		}
	}) 
}




// Retrieving All Active Products
module.exports.getAllActive = () =>{
	return Product.find({isActive:true}).then(result => result);
}

// Retrieving Specific Product
module.exports.getProduct = (productId) =>{
	return Product.findById(productId).then(result => result);
}

//update product information (admin only)

module.exports.updateProduct = (productId, reqBody) =>{
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		isActive: reqBody.isActive
	}

	return Product.findByIdAndUpdate(productId, updatedProduct).then((productUpdate, error) =>{
		if(error){
			return "product is not available";
		}
		else{
			return true
		}
	})
}

// Archiving Product

module.exports.archiveProduct = (productId) =>{
	let activeField = {
		isActive : false
	}

	return Product.findByIdAndUpdate(productId, activeField).then((isActivated, error) =>{

		if(error){
			return false;
		}
		else{
			return true
		}
	})
}
