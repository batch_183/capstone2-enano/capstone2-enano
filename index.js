const express = require ("express");
const mongoose = require ("mongoose");
const cors = require ("cors");

const userRoutes = require ("./route/userRoute");
const productRoutes = require ("./route/productRoute");
const orderRoutes = require("./route/orderRoute");

const app = express ();
const port = 4000;

//mongoose connection
mongoose.connect("mongodb+srv://admin:admin@myfirstcluster.gqz1k.mongodb.net/ecommerce_api?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});




//Notification for connection
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connnection error"));
db.once("open", ()=> console.log("We're connected to the cloud database"));

//Middlewares

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users",userRoutes);
// app.use("/products", productRoutes);
app.use("/products",productRoutes);
app.use("/orders", orderRoutes);

app.listen(port, () => console.log(`Server running at port ${port}`));