const express = require ("express");
const router = express.Router();
const productControllers = require ("../controller/productController");
const auth = require ("../auth");


//Creating a Product
router.post("/add", auth.verify, (req,res) =>{
    const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		productControllers.addProduct(req.body).then(resultFromController => res.send(resultFromController));
    }
	else{
		res.send("You don't have a permission to add a product!");
	}
})

//Retrieving all Active Products
router.get("/", (req, res) =>{
	productControllers.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Retrieving Specific Product
router.get("/:productId", (req, res)=>{
	console.log(req.params.productId);
	productControllers.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
})


// Route for updating a product (admin only)
 
router.put("/:productId", auth.verify, (req,res) =>{
    const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		productControllers.updateProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController));
    }
	else{
		res.send("You don't have a permission to update a product!");
	}
})

// Route to Archive a product (admin only)
router.patch("/:productId", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		productControllers.archiveProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
	}
	else {
		res.send("You don't have a permission to archive this product")
	}

})


module.exports = router;