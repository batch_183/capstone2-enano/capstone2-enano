const express = require("express");
const router = express.Router();
const userController = require("../controller/userController");
const auth = require ("../auth");


// User registration

router.post("/registration", (req, res) => {
    userController.registerUser (req.body).then(resultFromController => res.send(resultFromController));
});

//User loggin Authentication

router.post("/loginuser", (req, res)=>{
    userController.loginUser(req.body).then(resultFromController => res.send (resultFromController));
});


module.exports = router;