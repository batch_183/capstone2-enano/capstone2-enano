const express = require ("express");
const router = express.Router();
const orderController = require ("../controller/orderController");
const auth = require ("../auth");


//Creating an Order
router.post("/", (req, res) =>{  
		orderController.createOrder(req.body).then(resultFromController => res.send(resultFromController));
})

// retrieve all orders of authenticated users
router.get("/:usersorder", auth.verify, (req, res) =>{
    const userData = auth.decode(req.headers.authorization);
	if(!userData.isAdmin){
		orderController.usersorder().then(resultFromController => res.send(resultFromController));
		
	}    
	else{
		res.send("You don't have a permission to update view orders!");
	}
})

// retrieve all orders of admin

router.get("/", (req, res) =>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		orderController.getallorders().then(resultFromController => res.send(resultFromController));	

	}

	else {
		res.send(
			"You don't have permission to view the orders"
		)
	}
})

module.exports = router;

