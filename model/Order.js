const mongoose = require("mongoose");   

const orderSchema = new mongoose.Schema({  
    _id: {type: mongoose.Schema.Types.ObjectId},
    userId: {type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
    productId: {type: mongoose.Schema.Types.ObjectId, ref: "Product", required: true },
    quantity: {type: Number, default: 1}
    }, {timestamps: true})

module.exports = mongoose.model("Order", orderSchema);