const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: [true, "Email is required"]
    },
    password: {
        type: String,
        required: [true, "Password is required" ]
    },

    orders: [{
        productId : {type: String},
        quantity: {type: Number, default: 1},
        price: {type: Number}
    }],

    isActive: {
        type: Boolean,
        default: true
    },


    isAdmin: {
        type: Boolean,
        default: false
    }},

    {timestamps: true},
)

module.exports = mongoose.model("User", userSchema);